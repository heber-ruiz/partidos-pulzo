
start();

function start(){

        fetch('xml_to_json.php')
        .then((response)=> response.json())
        .then(data => {
        mostar_partidos(data.grupo_destacado['UEFA Champions League'], 'champions');
        mostar_partidos(data.grupo_destacado['Colombia - Primera División']['Deportivo Cali'], 'depCali');
        mostar_partidos(data.grupo_destacado['Colombia - Primera División']['Junior'], 'junior');
        mostrar_grupoCompleto(data.grupo_completo, 'grupoCompleto')
        });

}


function mostar_partidos(partidos, id) {
    let ul= document.createElement('ul');
    ul.classList.add('list-group');
    ul.classList.add('list-group-flush');

    partidos.forEach(partido => {
        let li= document.createElement('li');
        li.classList.add('list-group-item');
        li.classList.add('d-flex');
        li.classList.add('justify-content-around');

        var pattern = /(\d{4})(\d{2})(\d{2})/;
        var dt = new Date(partido.fecha.replace(pattern,'$1-$2-$3'));
        console.log(partido.fecha.replace(pattern,'$1-$2-$3'));
        li.innerHTML= `<img src="`+partido.local.escudo+`" width="128">
            <span class="text-center">`+ partido.local.fullName+' vs '+partido.visitante.fullName+`<br>`+partido.local.goles+' - '+partido.visitante.goles+
            `<br>` +dt.toDateString()+' '+partido.hora+`
            <br>`+partido.estado+
            ` </span>
            <img src="`+partido.visitante.escudo+`" width="128">`;


        ul.appendChild(li);

    });

    document.getElementById(id).appendChild(ul);
    
}

function mostrar_grupoCompleto(grupo, id){
    for( let x in grupo){
        let card= document.createElement('div');
        card.classList.add('card');
        card.classList.add('mb-2');
        card.innerHTML= `
        <div class="card-header" id="`+x+'_heading_'+`">
                `+x+`
        </div>

        <div class="card-body" id="`+x+'_liga__'+`">

        </div>`

        document.getElementById(id).appendChild(card);
        mostar_partidos(grupo[x], x+'_liga__');
        
    }

}