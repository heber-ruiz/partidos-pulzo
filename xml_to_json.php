<?php

function parse_xml(){
    $file_string = file_get_contents('https://filesstaticpulzo.s3.us-west-2.amazonaws.com/pulzo-lite/deportes.todos.agenda.pulzo.xml');
    $file_xml = simplexml_load_string($file_string);
    $info= [];
        
    foreach ($file_xml->children() as $partido) {
        if($partido->getName()=='partido'){
            $attr= $partido->attributes();
            $estado= '';
            $local=[];
            $visitante=[];
    
            foreach ($partido->children() as $hijo) {
                $attr_hijo= $hijo->attributes();
    
                if($hijo->getName()=='estado'){
                    $estado= $hijo->__toString();
                }
    
                $local= equipo( $local, $hijo, $attr_hijo, 'local');
                $visitante= equipo($visitante, $hijo, $attr_hijo, 'visitante');
    
            }
    
            $info[$attr['nombreCategoria']->__toString()][]=[
                'id' => $attr['id']->__toString(),
                'fecha' => $attr['fecha']->__toString(),
                'hora' => $attr['hora']->__toString(),
                'liga' => $attr['nombreCampeonato']->__toString(),
                'urlMatch' => $attr['canal']->__toString(),
                'estado' => $estado,
                'local' => $local ,
                'visitante' => $visitante,
            ];
        }
    }

    return $info;

}

function equipo($equipo, $element, $attr, $tipo){

    if($element->getName()==$tipo){
        $equipo= [
            'fullName' => $element->__toString(),
            'shortName' => $attr['nombreCorto']->__toString(),
            'escudo' => 'https://d330f5da3d0xn8.cloudfront.net/images/escudos/'.$attr['id']->__toString().'.gif',
            'goles' => '',
            'penales' => '',
        ];
    }

    if($element->getName()=='goles'.$tipo){
        $equipo['goles']= $element->__toString();
    }

    if($element->getName()=='golesDefPenales'.$tipo){
        $equipo['penales']= $element->__toString();
    }
    return $equipo;

}

function isCali($partido){
    if($partido['local']['fullName']=='Deportivo Cali' or $partido['visitante']['fullName']=='Deportivo Cali'){
        return 1;
    } else {
        return 0;
    }
}

function isJunior($partido){
    if($partido['local']['fullName']=='Junior' or $partido['visitante']['fullName']=='Junior'){
        return 1;
    } else {
        return 0;
    }
}

function main(){
    header('Content-Type: application/json');

    $listado= parse_xml();
    
    $listado_champions= $listado['UEFA Champions League'];
    
    $listado_liga_colombiana= [
        'Deportivo Cali' => array_values(array_filter($listado['Colombia - Primera División'], 'isCali')),
        'Junior' => array_values(array_filter($listado['Colombia - Primera División'], 'isJunior'))
    ];
    
    echo json_encode([
        'grupo_destacado' => ['UEFA Champions League' =>  $listado_champions, 'Colombia - Primera División' => $listado_liga_colombiana],
        'grupo_completo' => $listado
    ]);
}

main();



